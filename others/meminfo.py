#!/usr/bin/env python3.6

import subprocess
import re
import os

raw_data = str(subprocess.check_output(['dmidecode', '-t', 'memory']))

pattern = re.compile("Type: ([\w ]+)", re.DOTALL)
raw_res = pattern.findall(raw_data)
memory_type = "antani"
for elem in raw_res:
    if((str(elem) != "None") and (str(elem) != "Unknown")):
        memory_type = str(elem)
        exit
pattern = re.compile("Clock Speed: ([\w ]+)\ MHz")
memory_clock = pattern.findall(raw_data)
pattern = re.compile("Size: ([\w ]+)\ MB")
memory_size = pattern.findall(raw_data)
sticks_number = str(len(memory_size))

print("Memory type: " + memory_type +
      " - Size: " + memory_size[0] + "x" + sticks_number + " MB" +
      " - Clock speed: " + memory_clock[0] + " MHz")

print("os.sched " + str(len(os.sched_getaffinity(0))))
print("os.cpu_count()." + str(os.cpu_count()))
