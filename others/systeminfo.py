#!/usr/bin/env python3.6

import os
import platform
import multiprocessing
import time
import subprocess
import re


thread_count = os.cpu_count()
raw_data = str(subprocess.check_output(['lscpu']))
pattern = re.compile("Core\(s\) per socket: ([\w ]+)")
raw_res = pattern.findall(raw_data)
core_count = int(raw_res[0])


# Gather motherboard informations
# vendor: /sys/devices/virtual/dmi/id/board_vendor
# name: /sys/devices/virtual/dmi/id/board_name
# BIOS date: /sys/devices/virtual/dmi/id/bios_date
# BIOS version: /sys/devices/virtual/dmi/id/bios_version
f = open('/sys/devices/virtual/dmi/id/board_vendor', 'r')
board_vendor = f.read().rstrip()
f.close()
f = open('/sys/devices/virtual/dmi/id/board_name', 'r')
board_name = f.read().rstrip()
f.close()
f = open('/sys/devices/virtual/dmi/id/bios_date', 'r')
bios_date = f.read().rstrip()
f.close()
f = open('/sys/devices/virtual/dmi/id/bios_version', 'r')
bios_version = f.read().rstrip()
f.close()

sysinfo = "\n  CPU: " + platform.processor() + "\n"
sysinfo += "  Motherboard vendor: " + board_vendor + "\n"
sysinfo += ("  Motherboard model: " + board_name + 
            " - BIOS: " + bios_version + 
            " (" + bios_date + ")\n")
            
# Gather system memory informations 
raw_data = str(subprocess.check_output(['dmidecode', '-t', 'memory']))

pattern = re.compile("Type: ([\w ]+)", re.DOTALL)
raw_res = pattern.findall(raw_data)
for elem in raw_res:
    if((str(elem) != "None") and (str(elem) != "Unknown")):
        memory_type = str(elem)
        exit
pattern = re.compile("Clock Speed: ([\w ]+)\ MHz")
memory_clock = pattern.findall(raw_data)
pattern = re.compile("Size: ([\w ]+)\ MB")
memory_size = pattern.findall(raw_data)
sticks_number = str(len(memory_size))            

sysinfo += ("  Memory type: " + memory_type +
            " - Size: " + sticks_number + "x" + memory_size[0] + " MB" +
            " - Clock speed: " + memory_clock[0] + " MHz\n")

# Gather Cores frequency data
# /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq
cfreq = []
for i in range(0, thread_count):
    cfreq.append(open(str('/sys/devices/system/cpu/cpu' 
                          + str(i) + '/cpufreq/cpuinfo_cur_freq'), 
                          'r'))
    
# Gather Cores temperature data
# /sys/bus/platform/devices/coretemp.0/hwmon/hwmon1/temp2_input
ctemp = []
for i in range(2, int(core_count + 2)):
    ctemp.append(open(str('/sys/bus/platform/devices/coretemp.0' + 
                          '/hwmon/hwmon1/temp' + str(i) + '_input'), 
                          'r'))
                          
# Gather CPU core voltage
# /sys/class/hwmon/hwmon2/in0_input
vcore = open('/sys/class/hwmon/hwmon2/in0_input', 'r')

# Gather DIMM voltage
# /sys/class/hwmon/hwmon2/in6_input
vdimm = open('/sys/class/hwmon/hwmon2/in6_input', 'r')

ctemp_max = []
y = 0
for i in range(2, int(core_count + 2)):
    ctemp_max.append(int(int(ctemp[y].read().rstrip()) / 1000))
    #print("ctemp_max[y] = " + str(ctemp_max[y]))
    y += 1

try:    
    while True:
        os.system("clear")
        print(sysinfo)
        
        i = 0
        y = 0
        while i < thread_count:
            ctemp[y].seek(0)
            cfreq[i].seek(0)
            cfreq[i + 1].seek(0)
            freq1 = int(cfreq[i].read().rstrip()) / 1000
            freq2 = int(cfreq[i + 1].read().rstrip()) / 1000
            temp = int(int(ctemp[y].read().rstrip()) / 1000)
            if(temp > ctemp_max[y]):
                ctemp_max[y] = temp
            print("  core " + str(i) + ": " + 
                  format(float(freq1), '.2f') + 
                  " MHz   -   core " + str(i+1) + ": " + 
                  format(float(freq2), '.2f') + 
                  " MHz   ||   temp " + str(y) + ": " + str(temp) + 
                  " °C - max: " + str(ctemp_max[y]))
            i += 2
            y += 1

        print("\n  Vcore: " + 
              format(int(vcore.read().rstrip()) / 1000, '.3f') + 
              " Volt     -   " + 
              "VDIMM: " + 
              format(int(vdimm.read().rstrip()) / 1000, '.3f') + 
              " Volt")
        vcore.seek(0)
        vdimm.seek(0)
            
        time.sleep(1)
except KeyboardInterrupt:
    # Close open files
    for i in range(0, thread_count):
        cfreq[i].close()
    for i in range(0, core_count):
        ctemp[i].close()
    vcore.close()
    vdimm.close()
        
    print("Bye")
