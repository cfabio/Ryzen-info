#!/bin/sh

INSTALL_DIR="/opt/ryzen-info2"
SCRIPT_PATH="/usr/sbin/ryzen-info2"

mkdir $INSTALL_DIR
chmod 755 ryzen-info.py
cp ryzen-info.py $INSTALL_DIR
echo "sudo python3 $INSTALL_DIR/ryzen-info.py" > $SCRIPT_PATH
chmod 755 $SCRIPT_PATH
