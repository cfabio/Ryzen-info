# Ryzen-info

![screenshot](https://gitlab.com/cfabio/Ryzen-info/raw/master/screenshot.png)

# FAQ

## What is Ryzen-info?

It is a Python script that can be use to check AMD Ryzen platform
working frequencies, termperatures, voltage readings and some more.

## Dependecies
To work properly Ryzen-info requires the following packages to be 
installed and configured correctly:
* `python 3`
* `dmidecode`
* `lm_sensors`

## How do I install Ryzen-info?

You can run it simply by typing in your preferred terminal emulator:
`python3 ryzen-info`

Alternatively copy it in `/usr/sbin` or add its path to PATH env. var.

It also ships with two bash scripts, `install.sh` and `uninstall.sh`, 
those can be used to automagically install or uninstall the script.

## Why is running as root required?

Because certain information cannot be accessed without root priviledges,
 for example `dmidecode` command on certain distros requires root.

## I found a bug

Please report it using the [issue tracker][issues].

If you are experiencing misbehavior or crash please provide detailed 
steps on how to reproduce the bug. 

Always mention the Operating System you are using.

[issues]: https://gitlab.com/cfabio/Ryzen-info/issues
